module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),
/* 1 */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(3);


/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });

// EXTERNAL MODULE: external "react"
var external__react_ = __webpack_require__(0);
var external__react__default = /*#__PURE__*/__webpack_require__.n(external__react_);

// EXTERNAL MODULE: external "next/head"
var head_ = __webpack_require__(1);
var head__default = /*#__PURE__*/__webpack_require__.n(head_);

// CONCATENATED MODULE: ./components/Layout.js
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }




var Layout_Layout =
/*#__PURE__*/
function (_Component) {
  _inherits(Layout, _Component);

  function Layout() {
    _classCallCheck(this, Layout);

    return _possibleConstructorReturn(this, (Layout.__proto__ || Object.getPrototypeOf(Layout)).apply(this, arguments));
  }

  _createClass(Layout, [{
    key: "render",
    value: function render() {
      var props = this.props;
      return external__react__default.a.createElement("div", null, external__react__default.a.createElement(head__default.a, null, external__react__default.a.createElement("title", null, "BLN - PLATAFORMA EN MANTENIMIENTO"), external__react__default.a.createElement("link", {
        rel: "stylesheet",
        href: "/static/css/style.css"
      })), props.children, external__react__default.a.createElement("script", {
        src: "https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"
      }), external__react__default.a.createElement("script", {
        src: "/static/js/jquery.js"
      }), external__react__default.a.createElement("script", {
        src: "/static/js/scripts.js"
      }));
    }
  }]);

  return Layout;
}(external__react_["Component"]);

/* harmony default export */ var components_Layout = (Layout_Layout);
// CONCATENATED MODULE: ./pages/index.js




var pages_Index = function Index() {
  return external__react__default.a.createElement(components_Layout, null, external__react__default.a.createElement("div", {
    className: "page text-center"
  }, external__react__default.a.createElement("main", {
    className: "page-content",
    style: {
      background: 'url(static/img/background-17-1920x955.jpg) 0/cover no-repeat'
    }
  }, external__react__default.a.createElement("div", {
    className: "one-page"
  }, external__react__default.a.createElement("div", {
    className: "one-page-header"
  }, external__react__default.a.createElement("div", {
    className: "rd-navbar-brand"
  }, external__react__default.a.createElement("a", {
    href: "index.html"
  }, external__react__default.a.createElement("img", {
    style: {
      marginTop: '-5px',
      marginLeft: '-15px'
    },
    width: 138,
    src: "static/img/logo-white.png",
    alt: true
  })))), external__react__default.a.createElement("section", null, external__react__default.a.createElement("div", {
    className: "shell"
  }, external__react__default.a.createElement("div", {
    className: "range"
  }, external__react__default.a.createElement("div", {
    className: "range range-xs-center range-xs-middle section-110 section-cover context-dark"
  }, external__react__default.a.createElement("div", {
    className: "cell-xs-12"
  }, external__react__default.a.createElement("h3", {
    className: "text-center"
  }, "We're getting ready to launch in"), external__react__default.a.createElement("hr", {
    className: "divider divider-md bg-mantis"
  }), external__react__default.a.createElement("div", {
    className: "offset-top-34"
  }, external__react__default.a.createElement("div", {
    className: "countdown-custom countdown-ellipse"
  }, external__react__default.a.createElement("div", {
    className: "countdown",
    "data-type": "until",
    "data-time": "18 jul 2018",
    "data-format": "dhms"
  }))), external__react__default.a.createElement("p", {
    className: "offset-top-30 offset-sm-top-66"
  }, "Our website is under construction, we are working very hard to give you the best ", external__react__default.a.createElement("br", {
    className: "veil reveal-sm-inline-block"
  }), " experience on our new web site."), external__react__default.a.createElement("div", {
    className: "range range-sm-center offset-top-24"
  }, external__react__default.a.createElement("div", {
    className: "cell-sm-8 cell-md-6 cell-lg-4"
  }, external__react__default.a.createElement("small", null, "Stay ready, we`re launching soon")))))))), external__react__default.a.createElement("div", {
    className: "one-page-footer"
  }, external__react__default.a.createElement("p", {
    className: "small",
    style: {
      color: 'rgba(255,255,255, 0.3)'
    }
  }, "BLN \xA9 ", external__react__default.a.createElement("span", {
    className: "copyright-year"
  }), "2018"))))));
};

/* harmony default export */ var pages = __webpack_exports__["default"] = (pages_Index);

/***/ })
/******/ ]);
//# sourceMappingURL=index.js.map