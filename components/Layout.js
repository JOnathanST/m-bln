import React, { Component } from "react"; 
import Head from 'next/head';

class Layout extends Component { 
  
  	render() {  
	    const props = this.props;
	    return ( 
	      	<div>
		      	<Head>
		        	<title>BLN - PLATAFORMA EN MANTENIMIENTO</title>
        			<link rel="stylesheet" href="/static/css/style.css"/>
		      	</Head>
		      	{/* ============================================================== */}
		        {/* Container-Fluid */}
		        {/* ============================================================== */}
		        {props.children}
		        {/* ============================================================== */}
		        {/* Container-Fluid */}
		        {/* ============================================================== */}  
		        
      			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
      			<script src="/static/js/jquery.js"></script>
      			<script src="/static/js/scripts.js"></script>
	    	</div> 
	    );
  	}
} 
export default Layout;  