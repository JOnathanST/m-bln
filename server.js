const next = require('next')
const routes = require('./routes')
const app = next({dev: process.env.NODE_ENV !== 'production'})
const handler = routes.getRequestHandler(app)
var fs = require('fs')
var path = require('path')
const port = 5000;
// Create a token generator with the default settings:
var uid = require('rand-token').uid;
var token = uid(25);
var bodyParser = require('body-parser');
// With express
const express = require('express');

app.prepare().then(() => {
    const server = express()
  
    server.use(bodyParser.urlencoded({extended: true}));
    server.use(bodyParser.json());

   	server.get('*', (req, res) => { return app.render(req, res, '/', req.query) })

    server.listen(port, (err) => {
		if (err) throw err
		console.log('> Ready on http://localhost:' + port)
	})
}) 