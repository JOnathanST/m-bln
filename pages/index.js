
import Layout from '../components/Layout';  
import Head from 'next/head';

const Index = () => (
	<Layout>
		<div className="page text-center">
		  {/* Page Content*/}
		  <main className="page-content" style={{background: 'url(static/img/background-17-1920x955.jpg) 0/cover no-repeat'}}>
		    <div className="one-page">
		      <div className="one-page-header">
		        {/*Navbar Brand*/}
		        <div className="rd-navbar-brand"><a href="index.html"><img style={{marginTop: '-5px', marginLeft: '-15px'}} width={138} src="static/img/logo-white.png" alt /></a></div>
		      </div>
		      {/* Coming Soon*/}
		      <section>
		        <div className="shell">
		          <div className="range">
		            <div className="range range-xs-center range-xs-middle section-110 section-cover context-dark">
		              <div className="cell-xs-12">
		                <h3 className="text-center">We're getting ready to launch in</h3>
		                <hr className="divider divider-md bg-mantis" />
		                <div className="offset-top-34">
		                  {/* Countdown*/}
		                  <div className="countdown-custom countdown-ellipse">
		                    <div className="countdown" data-type="until" data-time="18 jul 2018" data-format="dhms" />
		                  </div>
		                </div>
		                <p className="offset-top-30 offset-sm-top-66">Our website is under construction, we are working very hard to give you the best <br className="veil reveal-sm-inline-block" /> experience on our new web site.</p>
		                <div className="range range-sm-center offset-top-24">
		                  <div className="cell-sm-8 cell-md-6 cell-lg-4"><small>Stay ready, we`re launching soon</small></div>
		                </div>
		              </div>
		            </div>
		          </div>
		        </div>
		      </section>
		      <div className="one-page-footer">
		        <p className="small" style={{color: 'rgba(255,255,255, 0.3)'}}>BLN © <span className="copyright-year" />2018</p>
		      </div>
		    </div>
		  </main>
		</div>

	</Layout>
);

export default Index;